# Task management system for final quadrant

# Requirements to run
* postgresql
* redis
* node.js

# Installation
* setup db as per config.js file using orm.db.connection data
* on redis server and postgresql service

# Steps to perform in terminal :
1. cd in the project dir and run npm install
2. npm run migrate latest
3. npm run server
4. open the browser and type localhost:3000

# Few assumption taken in consideration :
1. All user that will be added to project are that already present on the platform.
2. The user who create project has admin access level to that project.
3. Only admin can added user to project.
4. User with access level as employee for a project can only change the task status that too 'pending', 'assign', 'completed', 'blocked'.
5. User with access level as admin for a project can create, edit, delete or change status as required.
6. User will never forget his/her password.
